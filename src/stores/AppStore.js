const AppStore = {
	currentScreen: null,
	currentStep: null,

	investment: {

		goal: 'island',

		target: 100000,

		date: {
			year: 2017,
			month: 5
		},

		deposit: 1000,

		product:  '32 DAY FLEXI NOTICE',

	},

	setScreen: function (screen) {
		this.currentScreen = screen;
	},

	setStep: function (step) {
		AppStore.currentStep = step;
	},

	setGoal: (goal) => {
		AppStore.investment.goal = goal;
	},

	setTarget: (target) => {
		AppStore.investment.target = target;
	},

	setDate: (year, month) => {
		AppStore.investment.date.year = year;
		AppStore.investment.date.month = month;
	},

	setDeposit: (deposit) => {
		AppStore.investment.deposit = deposit;
	},

	setProduct: (product) => {
		AppStore.investment.product = product;
	},

	reset: function () {
		AppStore.investment.goal = null;
		AppStore.investment.target = null;
		AppStore.investment.date.year = null;
		AppStore.investment.date.month = null;
		AppStore.investment.deposit = null;
		AppStore.investment.product = null;
	}
};

export default AppStore;