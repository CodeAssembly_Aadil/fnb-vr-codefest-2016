export const TRAVEL = 'travel';
export const ISLAND = 'island';

export const WEDDING = 'wedding';
export const EVENT = 'event';

export const HOME_DEPOSIT = 'home_deposit';
export const CAR_DEPOSIT = 'car_deposit';
