// 1
export const SAVINGS_GOAL = 'savings-goal';
// 2
export const GOAL_AMOUNT = 'goal-amount';
// 3
export const GOAL_DATE = 'goal-date';
// 4
export const INITIAL_DEPOSIT = 'initial-deposit';
// 5
export const PRODUCT_SELECTION = 'product-selection';
// 6
export const CONFIRMATION = 'confirmation';
// 7
export const THANK_YOU = 'thank-you';
