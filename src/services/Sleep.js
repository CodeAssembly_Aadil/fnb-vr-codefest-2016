const Sleep = {

	_video: null,

	prevent: function () {
		console.log('Sleep.prevent');

		if (!this._video) {
			this._init();
		}

		this._video.setAttribute('loop', 'loop');
		this._video.play();
	},

	allow: function () {
		console.log('Sleep.allow');

		if (!this._video) {
			return;
		}

		this._video.removeAttribute('loop');
		this._video.pause();
	},

	_init: function () {
		this._video = document.createElement('video');
		this._video.setAttribute('width', '10');
		this._video.setAttribute('height', '10');
		this._video.style.position = 'absolute';
		this._video.style.top = '-10px';
		this._video.style.left = '-10px';

		const sourceMp4 = document.createElement('source');
		sourceMp4.setAttribute('src', 'static/video/muted-blank.mp4');
		sourceMp4.setAttribute('type', 'video/mp4');
		this._video.appendChild(sourceMp4);

		const sourceOgg = document.createElement('source');
		sourceOgg.setAttribute('src', 'static/video/muted-blank.ogv');
		sourceOgg.setAttribute('type', 'video/ogg');
		this._video.appendChild(sourceOgg);

		document.body.appendChild(this._video);
	},
};

export default Sleep;
