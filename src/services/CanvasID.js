let _canvansID = 0;

const generateCanvasID = () => {
	return _canvansID++;
};

export default generateCanvasID;
