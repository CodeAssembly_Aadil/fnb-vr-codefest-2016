export const formatCurrency = function (value) {

	let val = 0.00;

	if(isNumeric(value)) {
		val = parseFloat(value).toFixed(2);
	}

	if (val == 0) {
		return 'R 0.00';
	}

	let display = String(val);
	display = display.replace(/^0*/, '');
	display = display.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
	display = 'R ' + display;

	return display;
};

const isNumeric = function (n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};