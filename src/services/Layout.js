const RADIANS = (Math.PI / 180);

const Layout = {

	distributeAlongCircumference: function (offsetAngle, radius, index, total) {
		const degrees = -offsetAngle * ((total - 1) / 2) + (offsetAngle * index);
		const radians = degrees * (Math.PI / 180);

		const x = radius * Math.sin(radians);
		const z = radius * Math.cos(radians);

		return {x:x, y:z};
		return x + ' 0 ' + z;
	},

	rotateToCenter: function (offsetAngle, index, total) {
		const degrees = -offsetAngle * ((total - 1) / 2) + (offsetAngle * index);

		const y = (180 + degrees);

		return '0 ' + y + ' 0';
	},


	pointOnCircumference: function (angle, radius) {
		const x = radius * Math.sin(RADIANS * angle);
		const y = radius * Math.cos(RADIANS * angle);

		return {x:x, y:y};
	},

	pointOnSphere: function (horizontalAngle, verticalAngle, radius) {
		const x = radius * Math.sin(RADIANS * horizontalAngle);
		const y = radius * Math.cos(RADIANS * verticalAngle);
		const z = radius * Math.cos(RADIANS * horizontalAngle);

		return { x:x, y:y, z:z };
	},

	rotateToCenter3D: function (horizontalAngle, verticalAngle) {
		const degrees = -offsetAngle * ((total - 1) / 2) + (offsetAngle * index);

		const y = (180 + horizontalAngle);
		const z = (180 + verticalAngle);

		return { x:x, y:y, z:z };
	}
};

export default Layout;




// const Layout = {

// 	distributeAlongCircumference: function (offsetAngle, radius, index, total) {
// 		const degrees = -offsetAngle * ((total - 1) / 2) + (offsetAngle * index);
// 		const radians = degrees * (Math.PI / 180);

// 		const x = radius * Math.sin(radians);
// 		const z = radius * Math.cos(radians);

// 		return x + ' 0 ' + z;
// 	},

// 	rotateToCenter: function (offsetAngle, index, total) {
// 		const degrees = -offsetAngle * ((total - 1) / 2) + (offsetAngle * index);

// 		const y = (180 + degrees);

// 		return '0 ' + y + ' 0';
// 	}

// };

// export default Layout;
