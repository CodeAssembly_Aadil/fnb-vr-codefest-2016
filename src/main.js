import Vue from 'vue';
import App from 'App';

import * as GSAP from 'gsap';
import * as AFrame from 'aframe';

new Vue({
	el: '#app',
	render: h => h(App)
});